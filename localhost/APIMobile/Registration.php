<?php
require_once "dbhelper.php";

$postData = file_get_contents('php://input');
$dataArray = json_decode($postData, TRUE);

$login = $dataArray['Login'];
$password = $dataArray['Password'];
$name = $dataArray['Name'];

if($login == "" || $password == "" || $name == "") {
    echo json_encode("Заполнены не все поля");
    exit;
}
$passwordHash = sha1($password);
$connection = new DBHelper();
$repeat_users = json_decode($connection->query_get("select * from user where login = '$login'"), TRUE);
if(isset($repeat_users[0]['Login'])){
    echo json_encode("Пользователь с логином $login уже существует!");
}
else 
{
    //Запрос в бд о создании марки
    if($connection->query_put("insert into user values (NULL, 'user', '$name', '$login', '$passwordHash')", TRUE)) {
        echo json_encode("Пользователь зарегистрирован");
    }
    else{
        echo json_encode("Не удалось зарегистрировать пользователя. Неизвестная ошибка.");
    }
}
