<?php
require_once "../Controllers/php-jwt-master/src/JWT.php";
use Firebase\JWT\JWT;

class Bearer
{
    function Get_id($jwt) {
        $appsetting = file_get_contents('../appsetting.json');
        $tokenKey = json_decode($appsetting, TRUE)['appsetting']['secret_token_key'];

        $decoded = JWT::decode($jwt, $tokenKey, array('HS256'));

        foreach($decoded as $key => $value) 
        { 
            if($key == "id") {
                $id = $value;
            }
            if($key == "role") {
                $role = $value;
            }
        } 

        return $id;
    }

    function Get_role($jwt) {
        $appsetting = file_get_contents('../appsetting.json');
        $tokenKey = json_decode($appsetting, TRUE)['appsetting']['secret_token_key'];

        $decoded = JWT::decode($jwt, $tokenKey, array('HS256'));

        foreach($decoded as $key => $value) 
        { 
            if($key == "id") {
                $id = $value;
            }
            if($key == "role") {
                $role = $value;
            }
        } 

        return $role;
    }
}
?>
