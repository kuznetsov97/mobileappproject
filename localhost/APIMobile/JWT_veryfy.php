<?php
require_once "dbhelper.php";
require_once "../Controllers/php-jwt-master/src/JWT.php";
use Firebase\JWT\JWT;

        $appsetting = file_get_contents('../appsetting.json');
        $tokenKey = json_decode($appsetting, TRUE)['appsetting']['secret_token_key'];


        $postData = file_get_contents('php://input');
        $dataArray = json_decode($postData, TRUE);

        $jwt = $dataArray['jwt'];
        if($jwt == null)
        {
            echo json_encode(false);
            exit;
        }

        $decoded = JWT::decode($jwt, $tokenKey, array('HS256'));

        foreach($decoded as $key => $value) 
        { 
            if($key == "id") {
                $id = $value;
            }
            if($key == "role") {
                $role = $value;
            }
        } 

        $connection = new DBHelper();
        $user_in_db = json_decode($connection->query_get("select role from user where id = '$id'"), TRUE);
        $user_role = $user_in_db[0]['role'];

        if($role == $user_role && $user_role != null) {
            echo json_encode($user_role);
        }
        else {
            echo json_encode(false);
        }

?>