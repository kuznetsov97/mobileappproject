<?php

class DBHelper {

    function connect() {
        //считывание настроек
        $appsetting = file_get_contents('../appsetting.json');
        $settingArray = json_decode($appsetting, TRUE);

        //подключение к бд
        $dblink = mysql_connect($settingArray['appsetting']['server'], 
                                $settingArray['appsetting']['admin'], 
                                $settingArray['appsetting']['password']);

        mysql_set_charset ('utf8', $dblink);

        if (!($dblink)) {
            echo 'Подключение не выполнено';
            exit;
        }
        if (!(mysql_select_db ($settingArray['appsetting']['nameDb'], $dblink))) {
            echo 'База данных не может быть выбрана';
            exit;
        }
        return $dblink;
    }

    public function query_get($query) {
        //запрос
        $res = mysql_query ($query, $this->connect());
        if (!($res)) { 
            echo 'Запрос не вернул результатов';
            exit;
        }

        $answer = array();

        $rows = mysql_num_rows($res); // количество полученных строк
        for ($i = 0 ; $i < $rows ; ++$i)
        {
            $answer[] = mysql_fetch_assoc($res); 
        }
     
        return json_encode($answer);
    }

    public function query_put($query) {
        //запрос
        $res = mysql_query ($query, $this->connect());
        return true;
    }

}
?>