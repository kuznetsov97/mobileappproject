-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 25 2020 г., 16:37
-- Версия сервера: 5.6.37
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `auto_book_mobile`
--

-- --------------------------------------------------------

--
-- Структура таблицы `alarms`
--

CREATE TABLE `alarms` (
  `id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `describtion` text NOT NULL,
  `date_complete` date NOT NULL,
  `period_date` int(11) NOT NULL,
  `period_km` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `alarms`
--

INSERT INTO `alarms` (`id`, `auto_id`, `describtion`, `date_complete`, `period_date`, `period_km`) VALUES
(1, 1, 'Замена масла', '2011-03-20', 6, 8000),
(3, 1, 'енлсдеусррр', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `auto`
--

CREATE TABLE `auto` (
  `id` int(11) NOT NULL,
  `mark` text NOT NULL,
  `model` text NOT NULL,
  `year` int(11) NOT NULL,
  `num` varchar(9) NOT NULL,
  `enter` tinyint(1) NOT NULL,
  `photo` blob NOT NULL,
  `sumcost` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auto`
--

INSERT INTO `auto` (`id`, `mark`, `model`, `year`, `num`, `enter`, `photo`, `sumcost`, `user_id`) VALUES
(1, 'ВАЗ', '21124', 2007, 'В329РМ33', 1, '', 0, 3),
(2, 'ВАЗ', '2101', 1987, 'т256хс37', 0, '', 0, 3),
(3, 'ВАЗ', '21120', 2004, 'а370кт37', 0, '', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `unit_of_work`
--

CREATE TABLE `unit_of_work` (
  `id` int(11) NOT NULL,
  `auto_id` int(11) NOT NULL,
  `work_id` int(11) DEFAULT NULL,
  `Describtion` text NOT NULL,
  `date` date NOT NULL,
  `cost` int(11) NOT NULL,
  `worker` text NOT NULL,
  `km` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `unit_of_work`
--

INSERT INTO `unit_of_work` (`id`, `auto_id`, `work_id`, `Describtion`, `date`, `cost`, `worker`, `km`) VALUES
(1, 1, 2, 'Замена прокладки ГБЦ', '2018-11-03', 7000, 'Сервис', 98000),
(3, 1, 6, 'вотв', '0000-00-00', 15, 'аоп', 54);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role` varchar(5) NOT NULL,
  `name` text NOT NULL,
  `login` text NOT NULL,
  `hash_pass` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `role`, `name`, `login`, `hash_pass`) VALUES
(2, 'admin', 'Всея админ', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(3, 'user', 'Михаил', 'misha', '7fc7f9e73856bd42a257ce7aac54fc3687f7ad60');

-- --------------------------------------------------------

--
-- Структура таблицы `work_type`
--

CREATE TABLE `work_type` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `work_type`
--

INSERT INTO `work_type` (`id`, `name`) VALUES
(1, 'Подвеска'),
(2, 'Двигатель'),
(3, 'Трансмиссия'),
(4, 'Интерьер'),
(5, 'Экстерьер'),
(6, 'Прочее');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `alarms`
--
ALTER TABLE `alarms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auto_id` (`auto_id`);

--
-- Индексы таблицы `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `unit_of_work`
--
ALTER TABLE `unit_of_work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `work_id` (`work_id`),
  ADD KEY `auto_id` (`auto_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `work_type`
--
ALTER TABLE `work_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `alarms`
--
ALTER TABLE `alarms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `auto`
--
ALTER TABLE `auto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `unit_of_work`
--
ALTER TABLE `unit_of_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `alarms`
--
ALTER TABLE `alarms`
  ADD CONSTRAINT `alarms_ibfk_1` FOREIGN KEY (`auto_id`) REFERENCES `auto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auto`
--
ALTER TABLE `auto`
  ADD CONSTRAINT `auto_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `unit_of_work`
--
ALTER TABLE `unit_of_work`
  ADD CONSTRAINT `unit_of_work_ibfk_1` FOREIGN KEY (`work_id`) REFERENCES `work_type` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `unit_of_work_ibfk_2` FOREIGN KEY (`auto_id`) REFERENCES `auto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
