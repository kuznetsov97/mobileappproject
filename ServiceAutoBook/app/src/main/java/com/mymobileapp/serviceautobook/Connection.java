package com.mymobileapp.serviceautobook;

import android.app.Service;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mymobileapp.serviceautobook.DTO.autoDTO;
import com.mymobileapp.serviceautobook.DTO.userDTO;
import com.mymobileapp.serviceautobook.DTO.worksDTO;
import com.mymobileapp.serviceautobook.DTO.serviceDTO;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.ServiceWork;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Connection extends AsyncTask<String, Void, String> {

    public int userId = 0;
    public int currentAutoId;
    private autoDTO auto;
    private userDTO user;
    private worksDTO work;
    private serviceDTO service;

    public ArrayList<Auto> Autoes = new ArrayList<Auto>();
    public ArrayList<WorkUnit> Works = new ArrayList<WorkUnit>();
    public ArrayList<ServiceWork> Services = new ArrayList<ServiceWork>();

    private DatabaseAdapter adapter;

    public void Initialize() {
        user = new userDTO();
        user.id = userId;
    }
    @Override
    protected String doInBackground(String... path) {
        String content="";

        if(path[0] == "auth") {
            try {
                content = QueryAuthenticate();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<Integer>() {}.getType();
                    userId = new Gson().fromJson(content, listType);
                }
            } catch (Exception ex) {
                content = ex.getMessage();
            }

            content = "auth";
            return content;
        }
        if(path[0] == "getServices") {
            try {
                content = QueryGetServices();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<ArrayList<serviceDTO>>() {}.getType();
                    List<serviceDTO> myList = new Gson().fromJson(content, listType);
                    for (serviceDTO item : myList) {
                        Services.add(new ServiceWork(item.id, (int) currentAutoId, item.describtion, new Date(), item.period_date, item.period_km, "Прочее", 0, 0));
                    }
                }
            } catch (Exception ex) {
                content = ex.getMessage();
            }

            content = "getServices";
            return content;
        }
        if(path[0] == "insertIntoServices") {
            try{
                content = QueryAddServices();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "insertIntoServices";
            return content;
        }
        if(path[0] == "updateService") {
            try{
                content = QueryUpdateServices();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "updateService";
            return content;
        }
        if(path[0] == "deleteService") {
            try{
                content = QueryRemoveServices();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "deleteService";
            return content;
        }
        if(path[0] == "getAutoes") {
            try{
                content = QueryGetAutoes();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<ArrayList<autoDTO>>() {}.getType();
                    List<autoDTO> myList = new Gson().fromJson(content, listType);
                    for (autoDTO item : myList) {
                        Autoes.add(new Auto(item.id, item.mark, item.model, item.year, item.num, item.photo, item.sumcost, item.enter));
                    }
                }
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "getAutoes";
            return content;
        }
        if(path[0] == "updateAuto") {
            try{
                content = QueryUpdateAutoes();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "updateAuto";
            return content;
        }
        if(path[0] == "deleteAuto") {
            try{
                content = QueryRemoveAutoes();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "deleteAuto";
            return content;
        }
        if(path[0] == "getWorks") {
            try{
                content = QueryGetWorks();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<ArrayList<worksDTO>>() {}.getType();
                    List<worksDTO> myList = new Gson().fromJson(content, listType);
                    for (worksDTO item : myList) {
                        Works.add(new WorkUnit(item.id, currentAutoId, item.work_id, item.describtion, item.date, item.cost, item.worker, item.km));
                    }
                }
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "getWorks";
            return content;
        }
        if(path[0] == "insertIntoWorks") {
            try{
                content = QueryAddWorks();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "insertIntoWorks";
            return content;
        }
        if(path[0] == "updateWork") {
            try{
                content = QueryUpdateWorks();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "updateWork";
            return content;
        }
        if(path[0] == "deleteWork") {
            try{
                content = QueryRemoveWorks();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "deleteWork";
            return content;
        }
        if(path[0] == "getCurrentAuto") {
            try{
                content = QueryGetEnteredAuto();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<autoDTO>() {}.getType();
                    autoDTO myList = new Gson().fromJson(content, listType);
                    currentAutoId = myList.id;
                }
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "getCurrentAuto";
            return content;
        }
        if(path[0] == "setCurrentAuto") {
            try{
                content = QuerySetEnteredAuto();
            }
            catch(Exception e){
                content = e.getMessage();
            }

            content = "setCurrentAuto";
            return content;
        }

        return content;
    }

    public void GetWork(int id, String describtion, Date date, int cost, String worker, int km, int work_id) {
        work = new worksDTO();
        work.id = id;
        work.describtion = describtion;
        work.date = date;
        work.cost = cost;
        work.worker = worker;
        work.km = km;
        work.work_id = work_id;
    }
    public String QueryGetWorks() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/work/getworks.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", currentAutoId);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryAddWorks() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/work/insertwork.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("auto_id", currentAutoId);
            postDataParams.put("work_id", work.work_id);
            postDataParams.put("describtion", work.describtion);
            postDataParams.put("date", work.date);
            postDataParams.put("cost", work.cost);
            postDataParams.put("worker", work.worker);
            postDataParams.put("km", work.km);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryRemoveWorks() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/work/deletework.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", work.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryUpdateWorks() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/work/updatework.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", work.id);
            postDataParams.put("work_id", work.work_id);
            postDataParams.put("describtion", work.describtion);
            postDataParams.put("date", work.date);
            postDataParams.put("cost", work.cost);
            postDataParams.put("worker", work.worker);
            postDataParams.put("km", work.km);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }

    public void GetAuto(int id, String mark, String model, int year, String num, int enter, String photo, int sumcost) {
        auto = new autoDTO();
        auto.id = id;
        auto.mark = mark;
        auto.model = model;
        auto.year = year;
        auto.num = num;
        auto.enter = enter;
        auto.photo = photo;
        auto.sumcost = sumcost;
    }
    public String QueryGetAutoes() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/getautoes.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", user.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryAddAutoes() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/insertauto.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("mark", auto.mark);
            postDataParams.put("model", auto.model);
            postDataParams.put("year", auto.year);
            postDataParams.put("num", auto.num);
            postDataParams.put("user_id", user.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryRemoveAutoes() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/deleteauto.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", auto.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryUpdateAutoes() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/updateauto.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", auto.id);
            postDataParams.put("mark", auto.mark);
            postDataParams.put("model", auto.model);
            postDataParams.put("year", auto.year);
            postDataParams.put("num", auto.num);
            postDataParams.put("mark", auto.photo);
            postDataParams.put("cost", auto.sumcost);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryGetEnteredAuto() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/getenteredauto.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", user.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QuerySetEnteredAuto() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/auto/setenterauto.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("user_id", user.id);
            postDataParams.put("id", auto.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }

    public void GetService(int id, String describtion, Date date_complete, int period_date, int period_km) {
        service = new serviceDTO();
        service.id = id;
        service.describtion = describtion;
        service.date_complete = date_complete;
        service.period_date = period_date;
        service.period_km = period_km;
    }
    public String QueryGetServices() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/alarm/getalarms.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", currentAutoId);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryAddServices() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/alarm/insertalarm.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("auto_id", currentAutoId);
            postDataParams.put("describtion", service.describtion);
            postDataParams.put("date_complete", service.date_complete);
            postDataParams.put("period_date", service.period_date);
            postDataParams.put("period_km", service.period_km);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryRemoveServices() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/alarm/deletealarm.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", service.id);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryUpdateServices() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/alarm/updatealarm.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("id", service.id);
            postDataParams.put("describtion", service.describtion);
            postDataParams.put("date_complete", service.date_complete);
            postDataParams.put("period_date", service.period_date);
            postDataParams.put("period_km", service.period_km);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }

    public void GetUser(int id, String name, String login, String role, String password) {
        user = new userDTO();
        user.id = id;
        user.name = name;
        user.login = login;
        user.role = role;
        user.password = password;
    }
    public String QueryRegisterUser() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/registration.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("Login", user.login);
            postDataParams.put("Password", user.password);
            postDataParams.put("Name", user.name);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }
    public String QueryAuthenticate() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.37:82/apimobile/user/authenticate.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("Login", user.login);
            postDataParams.put("Password", user.password);

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }


    public void setParent(DatabaseAdapter parent) {
        adapter = parent;
    }
    @Override
    protected void onPostExecute(String content) {
    }
}

