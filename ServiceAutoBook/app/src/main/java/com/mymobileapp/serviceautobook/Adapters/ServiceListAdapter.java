package com.mymobileapp.serviceautobook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.ServiceWork;
import com.mymobileapp.serviceautobook.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ServiceListAdapter extends ArrayAdapter<ServiceWork> {
    private LayoutInflater inflater;
    private int layout;
    private List<ServiceWork> services;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    private DatabaseAdapter adapter;

    public ServiceListAdapter(Context context, int resource, List<ServiceWork> services, DatabaseAdapter adapter) {
        super(context, resource, services);
        this.adapter = adapter;
        this.services = services;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(this.layout, parent, false);

        LinearLayout mainLayoutView = (LinearLayout) view.findViewById(R.id.main_layout);
        TextView dateDoneView = (TextView) view.findViewById(R.id.date_done);
        TextView dateNextView = (TextView) view.findViewById(R.id.date_next);
        TextView descrView = (TextView) view.findViewById(R.id.descr_service);

        ServiceWork service = services.get(position);
        if(service.getIsOnlyEvent() == 1) {
            dateNextView.setText(formatter.format(service.getDateComplete()));
            dateDoneView.setText("---");
        }
        else {
            dateDoneView.setText(formatter.format(service.getDateComplete()));

            Calendar date = Calendar.getInstance();
            date.setTime(service.getDateComplete());
            date.add(Calendar.MONTH, service.getPeriodDate());
            Date nextDate = date.getTime();

            dateNextView.setText(formatter.format(nextDate));
        }

        descrView.setText(service.getDescription());

        int colorNum = 0;
        //Date now = new Date(); now.getTime();
        //date.setTime(now);
        //if(date.after(nextDate))
        //{
        //    colorNum = R.color.redColor;
        //}
        //else
        //{
        //    colorNum = R.color.greenColor;
        //}
        if(service.getIsSystem() == 1) {
            colorNum = R.color.lightGrey;
        }

        mainLayoutView.setBackgroundResource(colorNum);
        return view;
    }
}
