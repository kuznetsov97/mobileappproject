package com.mymobileapp.serviceautobook.Models;

import java.util.Date;

public class WorkUnit {

    private long Id;
    private int WorkTypeId;
    private int AutoId;

    private String Description;
    private Date Date;
    private int Cost;
    private String Worker;
    private int Km;

    public WorkUnit(long id, int autoId, int workTypeId, String description, Date date, int cost, String worker, int km){
        Id = id;
        WorkTypeId = workTypeId;
        AutoId = autoId;
        Description = description;
        Date = date;
        Cost = cost;
        Worker = worker;
        Km = km;
    }

    public long getId(){return Id;}
    public int getWorkTypeId(){return WorkTypeId;}
    public int getAutoId(){return AutoId;}

    public String getDescription(){return Description;};
    public Date getDate(){return Date;}
    public int getCost(){return Cost;}
    public String getWorker(){return Worker;}
    public int getKm(){return Km;}

    public void setDescription(String value){
        Description = value;}
    public void setDate(Date value){
        Date = value;}
    public void setCost(int value){
        if(value>=0) {
            Cost = value;
        }
    }
    public void setWorker(String value){
        Worker = value;
    }
    public void setKm(int value){
        if(value>0){
            Km = value;
        }
    }
}
