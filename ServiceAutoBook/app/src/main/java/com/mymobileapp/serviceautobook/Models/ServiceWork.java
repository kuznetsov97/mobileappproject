package com.mymobileapp.serviceautobook.Models;

import java.util.Date;

public class ServiceWork {

    private long Id;
    private int AutoId;

    private String Description;
    private Date DateComplete;

    private int PeriodDate;//количество месяцев
    private int PeriodKm;
    private String Type;

    private int IsOnlyEvent;
    private int IsSystem;

    public ServiceWork(long id, int autoId, String description, Date dateComplete, int periodDate, int periodKm, String type, int isOnlyEvent, int isSystem){
        Id = id;
        AutoId = autoId;
        Description = description;
        DateComplete = dateComplete;
        PeriodDate = periodDate;
        PeriodKm = periodKm;
        Type = type;
        IsOnlyEvent = isOnlyEvent;
        IsSystem = isSystem;
    }

    public long getId(){return Id;}
    public int getAutoId(){return AutoId;}

    public String getDescription(){return Description;};
    public Date getDateComplete(){return DateComplete;}
    public int getPeriodDate(){return PeriodDate;}
    public int getPeriodKm(){return PeriodKm;}
    public String getType(){return Type;}
    public int getIsOnlyEvent() {return IsOnlyEvent;}
    public int getIsSystem() {return IsSystem;}

    public void setType(String value) {Type = value; }
    public void setDescription(String value){
        Description = value;
    }
    public void setDateComplete(Date value){
        DateComplete = value;
    }
    public void setIsOnlyEvent(int value) { IsOnlyEvent = value; }

}
