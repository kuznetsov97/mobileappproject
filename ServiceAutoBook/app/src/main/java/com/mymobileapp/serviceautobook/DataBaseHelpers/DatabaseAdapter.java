package com.mymobileapp.serviceautobook.DataBaseHelpers;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.mymobileapp.serviceautobook.Connection;
import com.mymobileapp.serviceautobook.DTO.worksDTO;
import com.mymobileapp.serviceautobook.Models.*;

public class DatabaseAdapter {

    public DatabaseAdapter(int userId) {

        Works = new ArrayList<WorkUnit>();
        Services = new ArrayList<ServiceWork>();
        Autoes = new ArrayList<Auto>();
        TypesOfWork = new ArrayList<WorkType>();
        TypesOfWork.add(new WorkType(1, "Подвеска"));
        TypesOfWork.add(new WorkType(2, "Двигатель"));
        TypesOfWork.add(new WorkType(3, "Трансмиссия"));
        TypesOfWork.add(new WorkType(4, "Интерьер"));
        TypesOfWork.add(new WorkType(5, "Экстерьер"));
        TypesOfWork.add(new WorkType(6, "Прочее"));

        LoadDataFromDB(userId);
    }

    public void LoadDataFromDB(int userId) {

        int limitWaitConnection = 0;

        connect = new Connection();
        connect.userId = userId;
        connect.Initialize();
        try {
            connect.execute("getAutoes");
            while (connect.Autoes.isEmpty()) {
                Thread.sleep(1000);
                limitWaitConnection++;

                if(limitWaitConnection >5) break;
            }
        } catch (InterruptedException ex) {}
        Autoes = connect.Autoes;


        limitWaitConnection = 0;
        connect = new Connection();
        connect.Initialize();
        connect.currentAutoId = (int)getEnteredAuto().getId();
        try{
            connect.execute("getWorks");
            while(connect.Works.isEmpty())
            {
                Thread.sleep(1000);
                limitWaitConnection++;

                if(limitWaitConnection >3) break;
            }
        }
        catch(InterruptedException ex){}
        Works = connect.Works;

        limitWaitConnection = 0;
        connect = new Connection();
        connect.Initialize();
        connect.currentAutoId = (int)getEnteredAuto().getId();
        try{
            connect.execute("getServices");
            while(connect.Services.isEmpty())
            {
                Thread.sleep(1000);
                limitWaitConnection++;

                if(limitWaitConnection >3) break;
            }
        }
        catch(InterruptedException ex){}
        Services = connect.Services;
    }

    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    private Connection connect;
    public int UserId = 0;
    public List<WorkUnit> Works;
    public List<ServiceWork> Services;
    public List<Auto> Autoes;
    public List<WorkType> TypesOfWork;

    ///////////////////////////////////////////////////////////////////////////////

    public List<WorkUnit> getAllWorks() {
        return getWorksSort();
    }

    public List<WorkUnit> getWorksSort() {
        return quickSort(Works, 0, Works.size()-1);
    }

    /**
     * Быстрая сортировка
     * @param array сортируемый список
     * @param start начальный индекс
     * @param end конечный индекс
     */
    public List<WorkUnit> quickSort (List<WorkUnit> array, int start, int end) {
        if ( start >= end ) {
            return array;
        }

        int marker = start;
        for ( int i = start; i <= end; i++ ) {
            if ( array.get(i).getDate().getTime() <= array.get(end).getDate().getTime() ) {
                WorkUnit temp = array.get(marker); // swap
                WorkUnit temp1 = array.get(i);

                array.remove(marker);
                array.add(marker, temp1);

                array.remove(i);
                array.add(i, temp);

                marker += 1;
            }
        }

        int pivot = marker - 1;
        quickSort (array, start, pivot-1);
        quickSort (array, pivot+1, end);

        return array;
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Запись о ремонте" (UnitOfWork) айди которой приходит на вход
     * @throws ParseException
     */
    public WorkUnit getWorkById(long id) throws ParseException {
        for(WorkUnit item : Works){
            if(item.getId() == id)
                return item;
        }
        return null;
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public void insertUOW(WorkUnit work){
        Works.add(work);
        connect = new Connection();
        connect.Initialize();
        connect.currentAutoId = (int)getEnteredAuto().getId();
        connect.GetWork((int)work.getId(), work.getDescription(), work.getDate(), work.getCost(), work.getWorker(), work.getKm(), work.getWorkTypeId());
        connect.execute("insertIntoWorks");

    }

    /**
     * @param id айди сущности "Запись о ремонте" (UnitOfWork), которую нужно удалить из бд
     * @return Удаляет сущность "Запись о ремонте" (UnitOfWork) из бд
     */
    public void deleteUOW(long id){
        WorkUnit deleted = null;
        for(WorkUnit item : Works){
            if(item.getId() == id)
                deleted = item;
        }
        if (deleted != null)
            Works.remove(deleted);

        connect = new Connection();
        connect.Initialize();
        connect.GetWork((int)deleted.getId(), deleted.getDescription(), deleted.getDate(), deleted.getCost(), deleted.getWorker(), deleted.getKm(), deleted.getWorkTypeId());
        connect.execute("deleteWork");
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public void updateUOW(WorkUnit work){

        connect = new Connection();
        connect.Initialize();
        connect.GetWork((int) work.getId(), work.getDescription(), work.getDate(), work.getCost(), work.getWorker(), work.getKm(), work.getWorkTypeId());
        connect.execute("updateWork");
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получает из бд все сущности "Автомобиль" (auto)
     */
    public List<Auto> getAllAutoes() {
        return  Autoes;
    }

    /**
     * @return Получает из бд выбранную сущность "Автомобиль" (auto)
     */
    public Auto getEnteredAuto() {
        for(Auto item : Autoes) {
            if(item.getEntered() == 1) {
                return item;
            }
        }
        return null;
    }

    /**
     * Изменяет автобобиль который выбран в данный момент
     * @param id айди авто, который только что выбран
     */
    public void setEnteredAuto(long id) {
        Auto old = getEnteredAuto();
        Auto now = getAutoById(id);

        if(old != null) {
            old.setEntered(0);
        }
        if(now != null) {
            now.setEntered(1);
        }
        connect = new Connection();
        connect.Initialize();
        connect.currentAutoId = (int)getEnteredAuto().getId();
        connect.GetAuto((int)now.getId()," "," ",0,"",0,"", 0);
        connect.execute("setCurrentAuto");
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Автомобиль" (auto) айди которой приходит на вход
     */
    public Auto getAutoById(long id){
        for(Auto item : Autoes){
            if(item.getId() == id)
                return item;
        }
        return null;
    }

    /**
     * @param auto Объект "Автомобиль" (auto), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Автомобиль" (auto)
     */
    public void insertAuto(Auto auto){
        Autoes.add(auto);
    }

    /**
     * @param id айди сущности "Автомобиль" (auto), которую нужно удалить из бд
     * @return Удаляет сущность "Автомобиль" (auto) из бд
     */
    public void deleteAuto(long id){
        Auto deleted = null;
        for(Auto item : Autoes){
            if(item.getId() == id)
                deleted = item;
        }
        if (deleted != null)
            Autoes.remove(deleted);

        connect = new Connection();
        connect.Initialize();
        connect.GetAuto((int)deleted.getId(), "", "", 0, "", 0, "", 0);
        connect.execute("deleteAuto");
    }

    /**
     * @param auto Объект "Автомобиль" (auto), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Автомобиль" (auto)
     */
    public void updateAuto(Auto auto){
        connect = new Connection();
        connect.Initialize();
        connect.GetAuto((int)auto.getId(), auto.getMark(), auto.getModel(), auto.getYear(), auto.getNumber(), auto.getEntered(), auto.getPhotoPath(), auto.getSumCost());
        connect.execute("updateAuto");
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получить все объекты "Тип работы" из бд
     */
    public List<WorkType> getAllTypesOfWork() {
        return TypesOfWork;
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Тип работы" (WorkType) айди которой приходит на вход
     * @throws ParseException
     */
    public WorkType getWorkTypeById(long id){
        for(WorkType item : TypesOfWork) {
            if(item.getId() == id)
                return item;
        }
        return null;
    }

    /**
     * @param name Параметр, по которому ищется запись в бд
     * @return "Тип работы" (WorkType) название которой приходит на вход
     * @throws ParseException
     */
    public WorkType getWorkTypeByName(String name){
        for(WorkType item : TypesOfWork) {
            if(item.getName() == name)
                return item;
        }
        return null;
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получает из бд сущности "Сервисная запись" (ServiceWork) для выбранного автомобиля
     */
    public List<ServiceWork> getAllServiceWorks() {
        return  Services;
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Сервисная запись" (ServiceWork) айди которой приходит на вход
     * @throws ParseException
     */
    public ServiceWork getServiceWorkById(long id) throws ParseException {
        for(ServiceWork item : Services) {
            if(item.getId() == id)
                return item;
        }
        return null;
    }

    /**
     * @param work Объект "Сервисная запись" (ServiceWork), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Сервисная запись" (ServiceWork)
     */
    public void insertSW(ServiceWork work){
        Services.add(work);

        connect = new Connection();
        connect.Initialize();
        connect.currentAutoId = (int)getEnteredAuto().getId();
        connect.GetService((int)work.getId(), work.getDescription(), work.getDateComplete(), work.getPeriodDate(), work.getPeriodKm());
        connect.execute("insertIntoServices");
    }

    /**
     * @param id айди сущности "Сервисная запись" (ServiceWork), которую нужно удалить из бд
     * @return Удаляет сущность "Сервисная запись" (ServiceWork) из бд
     */
    public void deleteSW(long id){
        ServiceWork deleted = null;
        for(ServiceWork item : Services)
            if(item.getId() == id) {
                deleted = item;
                break;
            }
        if(deleted != null)
            Services.remove(deleted);

        connect = new Connection();
        connect.Initialize();
        connect.GetService((int)deleted.getId(), deleted.getDescription(), deleted.getDateComplete(), deleted.getPeriodDate(), deleted.getPeriodKm());
        connect.execute("deleteService");
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public void updateSW(ServiceWork work){
        connect = new Connection();
        connect.Initialize();
        connect.GetService((int)work.getId(), work.getDescription(), work.getDateComplete(), work.getPeriodDate(), work.getPeriodKm());
        connect.execute("updateService");
    }

}
